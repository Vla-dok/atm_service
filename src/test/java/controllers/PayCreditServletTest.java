package controllers;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import services.ApplicationService;

import java.io.IOException;
import java.math.BigDecimal;

import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PayCreditServletTest {
    @Mock
    private ApplicationService service;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;

    @InjectMocks
    private PayCreditServlet servlet;

    @Test
    public void doGetPayCreditTest() throws ServletException, IOException {
        //given
        User user = new User(1L, "Ivan", "Ivanov");
        String path = "view/payCredit.jsp";

        when(request.getParameter("id")).thenReturn("1");
        when(request.getRequestDispatcher(path)).thenReturn(dispatcher);
        when(service.getUserById(1)).thenReturn(user);
        when(request.getRequestDispatcher(path)).thenReturn(dispatcher);
        //when
        servlet.doGet(request, response);
        //then
        verify(service, atLeast(1)).getUserById(1);
        verify(request, atLeast(1)).setAttribute("user", user);
        verify(request, atLeast(1)).getRequestDispatcher(path);
        verify(dispatcher, atLeast(1)).forward(request, response);
    }

    @Test
    public void doPostPayCreditTest() throws IOException, ServletException {
        //given
        BigDecimal amount = new BigDecimal(10);
        when(request.getParameter("id")).thenReturn("1");
        when(request.getParameter("amount")).thenReturn("10");
        when(request.getContextPath()).thenReturn("test");
        when(service.getAmountOfCredits(1)).thenReturn(amount);
        //when
        servlet.doPost(request, response);
        //then
        verify(request, atLeast(1)).getParameter("id");
        verify(request, atLeast(1)).getParameter("amount");
        verify(service, atLeast(1)).payCredit(1, amount);
        verify(response, atLeast(1)).sendRedirect("test/service");
    }
}