package controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;
import models.Card;
import models.User;
import services.ApplicationService;

@Log4j
@AllArgsConstructor
@NoArgsConstructor
@WebServlet("/payCredit")
public class PayCreditServlet extends HttpServlet {
    private ApplicationService applicationService;

    @Override
    public void init() {
        applicationService = new ApplicationService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info(String.format("METHOD:%s STATUS:%s URI:%s LOCALE:%s SESSION_ID:%s",
                req.getMethod(), resp.getStatus(), req.getRequestURI(), resp.getLocale(), req.getRequestedSessionId()));
        Card card = (Card) req.getSession().getAttribute("approvedCard");
        User user = applicationService.getUserById(card.getId_user());
        if (user != null) {
            req.setAttribute("user", user);
            req.getRequestDispatcher("view/payCredit.jsp").forward(req, resp);
        } else {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, String.format("NO SUCH USER WITH ID %d", card.getId_user()));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.info(String.format("METHOD:%s STATUS:%s URI:%s LOCALE:%s SESSION_ID:%s",
                req.getMethod(), resp.getStatus(), req.getRequestURI(), resp.getLocale(), req.getRequestedSessionId()));
        long id = Long.parseLong(req.getParameter("id"));
        BigDecimal amount = new BigDecimal(req.getParameter("amount"));

        if (applicationService.getAmountOfCredits(id).compareTo(amount) < 0) {
            req.setAttribute("error_amount", "You can`t pay more than you take");
            req.getRequestDispatcher("view/payCredit.jsp").forward(req, resp);
        } else {
            applicationService.payCredit(id, amount);
            resp.sendRedirect(req.getContextPath() + "/service");
        }
    }

    @Override
    public void destroy() {
        applicationService.destroy();
    }
}