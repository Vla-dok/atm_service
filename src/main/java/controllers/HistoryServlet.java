package controllers;

import java.util.Comparator;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;
import models.Card;
import models.Transaction;
import models.User;
import services.ApplicationService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Log4j
@AllArgsConstructor
@NoArgsConstructor
@WebServlet("/history")
public class HistoryServlet extends HttpServlet {
    private ApplicationService histService;

    @Override
    public void init() {
        histService = new ApplicationService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info(String.format("METHOD:%s STATUS:%s URI:%s LOCALE:%s SESSION_ID:%s",
                req.getMethod(), resp.getStatus(), req.getRequestURI(), resp.getLocale(), req.getRequestedSessionId()));
       // List<Transaction> histList = histService.getAllTransactions();
        Card card = (Card) req.getSession().getAttribute("approvedCard");
        List<Transaction> histListId = histService.getTransactionsByCardId(card.getId())
                .stream()
                .sorted(Comparator.comparingLong(Transaction::getId).reversed())
                .collect(Collectors.toList());;
        //req.setAttribute("histList", histList);
        req.setAttribute("histListId", histListId);
        req.getRequestDispatcher("view/History.jsp").forward(req, resp);
    }

    @Override
    public void destroy() {
        histService.destroy();
    }
}
